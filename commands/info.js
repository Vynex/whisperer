import { EmbedBuilder, SlashCommandBuilder } from "discord.js";
import { getServerInfo } from "../library/index.js";

const infoCommand = {
  data: new SlashCommandBuilder()
  .setName("info")
  .setDescription("Fetch Information about a CS:GO Server")
  .addStringOption((option) => option
    .setName("ip")
    .setDescription("IP Address of the CS:GO Server")
    .setRequired(true)
  )
  .toJSON(),

  execute: async (interaction) => {
    const ip = interaction.options.getString("ip").trim();
    const info = await getServerInfo(ip);

    const embed = new EmbedBuilder()
      .setColor(0xE7A016)
      .setTitle(info.Name)
      .setURL(`https://steam-redirect-dzm.pages.dev/?ip=${ip}`)
      .setDescription(`connect ${ip}`)
      .setThumbnail("https://i.imgur.com/VNk5wlL.png")
      .addFields(
        { name: "\u200B", value: "\u200B" },
        { name: "Current Map", value: info.Map, inline: true },
        { name: "Players", value: `${info["Players"]}/${info["Max. Players"]}`, inline: true },
        { name: "VAC", value: info.VAC ? "Secured" : "Unsecured", inline: true },
      )
      .setTimestamp();

		await interaction.reply({ embeds: [ embed ] });
  }
};

export default infoCommand;
