import { Client, Events, GatewayIntentBits, Collection, SlashCommandBuilder, REST, Routes } from "discord.js";
import * as dotenv from "dotenv";

import info from "./commands/info.js";
dotenv.config();

const client = new Client({ intents: [GatewayIntentBits.Guilds] });
const rest = new REST({ version: "10" }).setToken(process.env.BOT_TOKEN);

const commands = [ info.data ];
await rest.put(Routes.applicationCommands(process.env.CLIENT_ID), { body: commands });

client.commands = new Collection();
client.commands.set("info", info);

client.on(Events.ClientReady, () => {
	console.log(`Logged in as ${client.user.tag}!`);
});

client.on(Events.InteractionCreate, async (interaction) => {
	if (!interaction.isChatInputCommand()) return;
  const command = client.commands.get(interaction.commandName);
  if (!command) return;

  try {
		await command.execute(interaction);
	} catch (error) {
		console.error(error);
		if (interaction.replied || interaction.deferred) {
			await interaction.followUp({ content: "There was an error while executing this command!", ephemeral: true });
		} else {
			await interaction.reply({ content: "There was an error while executing this command!", ephemeral: true });
		}
	}
});

client.login(process.env.BOT_TOKEN);
