import dgram from 'node:dgram';
import { infoParser } from "./parser.js";

const sendRequest = async (ip, port) => {
  const client = dgram.createSocket('udp4');

  return new Promise((resolve, reject) => {
    const timeout = setTimeout(() => {
      reject();
    }, 5000);

    client.on("message", (message) => {
      client.close();
      clearTimeout(timeout);
      resolve(infoParser(message));
    });

    client.on("error", (error) => {
      client.close();
      clearTimeout(timeout);
      reject(error);
    });

    const INFO_COMMAND = Buffer.concat([
      Buffer.from([ 0xFF, 0xFF, 0xFF, 0xFF ]),
      Buffer.from([ 0x54 ]),
      Buffer.from("Source Engine Query", "ascii"),
      Buffer.from([ 0x00 ]),
    ]);

    client.send(INFO_COMMAND, port, ip, (error, bytes) => {
      if (error) console.error(error);
      // else console.log("->", bytes);
    });
  });
};

export default sendRequest;