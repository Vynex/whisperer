import { validateIP } from "./helpers.js";
import sendRequest from "./client.js";

export const getServerInfo = async (ip) => {
  return new Promise((resolve, reject) => {
    const split = ip.trim().split(":");

    if (split.length !== 2) reject("Invalid IP");
    else {
      const [ ip, port ] = split;

      if (!validateIP(ip)) reject("Invalid IP");
      else resolve(sendRequest(String(ip), Number(port)));
    }
  });
};
