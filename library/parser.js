import { readByte, readShort, readString } from "./utils.js";

const METHODS = {
  "byte": readByte,
  "string": readString,
  "short": readShort,
};

const FORMATS = {
  PREFIX:       {  skip: 4  },
  PREFIX_D:     {  type: "byte"  },
  HEADER:       {  type: "byte",     parser: "",   name: "Header"          },
  PROTOCOL:     {  type: "byte",     parser: "",   name: "Protocol"        },
  NAME:         {  type: "string",   parser: "",   name: "Name",           },
  MAP:          {  type: "string",   parser: "",   name: "Map",            },
  FOLDER:       {  type: "string",   parser: "",   name: "Folder",         },
  GAME:         {  type: "string",   parser: "",   name: "Game",           },
  ID:           {  type: "short",    parser: "",   name: "AppID",          },
  PLAYERS:      {  type: "byte",     parser: "",   name: "Players",        },
  MAX_PLAYERS:  {  type: "byte",     parser: "",   name: "Max. Players",   },
  BOTS:         {  type: "byte",     parser: "",   name: "Bots",           },
  SERVER_TYPE:  {  type: "byte",     parser: "",   name: "Server Type",    },
  ENVIRONMENT:  {  type: "byte",     parser: "",   name: "Environment",    },
  VISIBILITY:   {  type: "byte",     parser: "",   name: "Visibility",     },
  VAC:          {  type: "byte",     parser: "",   name: "VAC",            },
  VERSION:      {  type: "string",   parser: "",   name: "Version",        },
  EDF:          {  type: "byte"  },
};

const SOURCE_TV_FORMATS = {
  SOURCE_TV_PORT: { name: "Port", type: "short" },
  SOURCE_TV_NAME: { name: "Name", type: "short" },
};

const EDF_FORMATS = {
  PORT:       { name: "Port", flag: 0x80, type: "short" },
  STEAM_ID:   { name: "Steam ID", flag: 0x10, type: "short" },
  SOURCE_TV:  { name: "Source TV", flag: 0x40, formats: [ SOURCE_TV_FORMATS.SOURCE_TV_PORT, SOURCE_TV_FORMATS.SOURCE_TV_NAME ] },
  KEYWORDS:   { name: "Keywords", flag: 0x20, type: "short" },
  GAME_ID:    { name: "Game ID", flag: 0x01, type: "short" },
};

const INFO_DATA_FORMAT = [
  FORMATS.PREFIX,

  FORMATS.PROTOCOL,
  FORMATS.HEADER,
  FORMATS.NAME,
  FORMATS.MAP,
  FORMATS.FOLDER,
  FORMATS.GAME,
  FORMATS.ID,
  FORMATS.PLAYERS,
  FORMATS.MAX_PLAYERS,
  FORMATS.BOTS,
  FORMATS.SERVER_TYPE,
  FORMATS.ENVIRONMENT,
  FORMATS.VISIBILITY,
  FORMATS.VAC,
  FORMATS.VERSION,
];

const INFO_EXTRA_DATA_FORMAT = [
  EDF_FORMATS.PORT,
  EDF_FORMATS.STEAM_ID,
  EDF_FORMATS.SOURCE_TV,
  EDF_FORMATS.KEYWORDS,
  EDF_FORMATS.GAME_ID,
];

/**
 * @param {Buffer} buffer
 */
const infoParser = (buffer) => {
  const response = {};
  let pointer = 0;

  const looper = ({ flag, name, type }) => {
    // if (flag)

    const { data, length } = METHODS[type](pointer, buffer);
    if (name) response[name] = data;
    pointer += length;

    return data;
  };

  INFO_DATA_FORMAT.forEach((format) => {
    if (format.skip) pointer += format.skip;
    else {
      if (format === FORMATS.EDF) {
        //
      }

      looper(format);
    }
  });

  return response;
};

export {
  infoParser,
};
