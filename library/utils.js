/**
 * @param {Number} offset
 * @param {Buffer} buffer
 */
const readByte = (offset, buffer) => {
  return { data: buffer.readUInt8(offset), length: 1 };
};

/**
 * @param {Number} offset
 * @param {Buffer} buffer
 */
const readShort = (offset, buffer) => {
  return { data: buffer.readUInt16LE(offset), length: 2 };
};

/**
 * @param {Number} offset
 * @param {Buffer} buffer
 * @returns {String}
 */
const readString = (offset, buffer) => {
  let length = 0;

  let input = buffer.subarray(offset);
  const string = [];

  for (const [ _, byte ] of input.entries()) {
    length += 1;

    if (byte === 0x00) break;
    string.push(byte);
  }

  const data = new TextDecoder("utf-8").decode(new Uint8Array(string));
  return { data, length };
};

export {
  readByte,
  readShort,
  readString,
}
